package net.rphx.ttt;

import net.rphx.ttt.api.Gameplay;
import net.rphx.ttt.api.GameplayInteractor;
import net.rphx.ttt.api.RoomInteractor;
import net.rphx.ttt.api.entities.Board;
import net.rphx.ttt.api.entities.Move;
import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.api.storage.DataProviders;
import net.rphx.ttt.api.storage.PlayerProvider;
import net.rphx.ttt.stub.Fakes;
import net.rphx.ttt.stub.NullGameplayProvider;
import net.rphx.ttt.stub.NullRoomProvider;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class IntegrationTest {

    @Test
    public void testMakeMove(){
        Room room1 = new Room();
        room1.setId("1");
        Room room2 = new Room();
        room2.setId("2");
        Room room3 = new Room();
        room3.setId("3");
        List<Room> rooms = asList(room1, room2, room3);

        RoomInteractor roomInteractor = new RoomInteractor(new NullRoomProvider() {
            @Override
            public List<Room> getRooms(Player player) {
                return rooms;
            }
        });
        Board board1 = new Board(10);

        GameplayInteractor gameplayInteractor = new GameplayInteractor(new NullGameplayProvider() {
            @Override
            public Board getBoard(Player player, Room room) {
                return room == room1 ? board1 : null;
            }

            @Override
            public Board storeBoard(Player player, Room room, Board board) {
                return room == room1 ? board1 : null;
            }
        });

        Gameplay gameplay = new Gameplay(new DataProviders() {
            @Override
            public PlayerProvider<String> getPlayerProvider() {
                return null;
            }

            @Override
            public RoomInteractor getRoomInteractor() {
                return roomInteractor;
            }

            @Override
            public GameplayInteractor getGameplayInteractor() {
                return gameplayInteractor;
            }
        });


        Move move = new Move();
        //set X at (1,2)
        move.setX(1);
        move.setY(2);
        move.setXValue(true);
        move.setUserId(Fakes.fakePlayer().getId());
        move.setTime(System.currentTimeMillis());

        room1.addPlayer(Fakes.fakePlayer());

        gameplay.addMove(Fakes.fakePlayer(), room1, move);

        assertThat(board1.getValue(1, 2), is(equalTo(Board.Sign.X)));
        assertThat(room1.getLastPlayer(), is(equalTo(Fakes.fakePlayer().getId())));
        assertThat(room1.getLastMoveTime(), is(equalTo(move.getTime())));
    }
}
