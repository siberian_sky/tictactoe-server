package net.rphx.ttt;

import net.rphx.ttt.api.WinChecker;
import net.rphx.ttt.api.entities.Board;
import net.rphx.ttt.impl.firebase.BoardSerializer;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;


public class BoardTest {

    @Test
    public void testWinnerCheck() {
        testNoWinner("---"
                + "---"
                + "---");
        testNoWinner("x-x"
                + "x-x"
                + "-x-");
        testNoWinner("xox"
                + "xox"
                + "-x-");
        testNoWinner("xox"
                + "xox"
                + "ox-");
        testResult("xox"
                + "xox"
                + "-xx", Board.Sign.X, Board.Sign.X);
        testResult("xox"
                + "xox"
                + "-oo", Board.Sign.O, Board.Sign.O);

        testResult("xoo"
                + "xox"
                + "oxo", Board.Sign.O, Board.Sign.O);

        testResult("xox"
                + "xox"
                + "oxo", Board.Sign.O, Board.Sign.NONE);
    }

    private void testNoWinner(String data) {
        Board board = readBoard(data);
        assertThat(WinChecker.INSTANCE.checkWinner(board, Board.Sign.X), is(nullValue()));
    }

    private Board readBoard(String data) {
        Board board = new Board((int) Math.sqrt(data.length()));
        BoardSerializer.readBoard(board, data);
        return board;
    }

    private void testResult(String data, Board.Sign move, Board.Sign expected) {
        Board board = readBoard(data);
        assertThat(WinChecker.INSTANCE.checkWinner(board, move), is(expected));
    }

    @Test
    public void testGetSet() {
        String original = "xox"
                + "xox"
                + "-xx";
        Board board = readBoard(original);
        assertThat(board.getSize(), is(equalTo((int) Math.sqrt(original.length()))));
        String packed = BoardSerializer.writeBoard(board);
        assertThat(packed, is(equalTo(original)));
    }
}
