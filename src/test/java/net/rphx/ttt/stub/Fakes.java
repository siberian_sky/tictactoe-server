package net.rphx.ttt.stub;

import net.rphx.ttt.api.entities.Player;

public class Fakes {

    private static final Player FAKE_PLAYER = new Player("123456", "fake");

    private Fakes() {
    }

    public static Player fakePlayer() {
        return FAKE_PLAYER;
    }
}
