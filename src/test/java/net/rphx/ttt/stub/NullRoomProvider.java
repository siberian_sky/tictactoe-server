package net.rphx.ttt.stub;

import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.api.storage.RoomProvider;

import java.util.List;

public class NullRoomProvider implements RoomProvider{
    @Override
    public List<Room> getRooms(Player player) {
        return null;
    }

    @Override
    public Room newRoom(Player player, Room room) {
        return null;
    }

    @Override
    public Room store(Player player, Room room) {
        return null;
    }
}
