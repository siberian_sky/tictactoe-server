package net.rphx.ttt.stub;

import net.rphx.ttt.api.entities.Board;
import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.api.storage.GameplayProvider;

public class NullGameplayProvider implements GameplayProvider {

    @Override
    public Board getBoard(Player player, Room room) {
        return null;
    }

    @Override
    public Board storeBoard(Player player, Room room, Board board) {
        return null;
    }
}
