package net.rphx.ttt;

import net.rphx.ttt.api.DataStorageException;
import net.rphx.ttt.api.GameplayInteractor;
import net.rphx.ttt.api.entities.Board;
import net.rphx.ttt.api.entities.Move;
import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.stub.NullGameplayProvider;
import net.rphx.ttt.stub.Fakes;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class GameplayLogicTest {

    @Test
    public void testGetBoard() {
        Room room1 = new Room();
        room1.setId("1");

        Board board1 = new Board(10);
        GameplayInteractor gameplayInteractor = new GameplayInteractor(new NullGameplayProvider() {
            @Override
            public Board getBoard(Player player, Room room) {
                return room == room1 ? board1 : null;
            }
        });
        Board board = gameplayInteractor.getBoard(Fakes.fakePlayer(), room1);
        assertThat(board, is(board1));
    }

    @Test
    public void testCreateBoard() {
        Room room1 = new Room();
        room1.setId("1");

        Board board1 = new Board(10);
        GameplayInteractor gameplayInteractor = new GameplayInteractor(new NullGameplayProvider() {
            @Override
            public Board storeBoard(Player player, Room room, Board board) {
                return room == room1 ? board1 : null;
            }
        });
        Board board = gameplayInteractor.getBoard(Fakes.fakePlayer(), room1);
        assertThat(board, is(board1));
    }

    @Test
    public void testMakeMove() {
        Room roomOrig = new Room();
        roomOrig.setId("1");

        Room room1 = new Room();
        room1.setId("1");

        assertThat(room1, is(equalTo(roomOrig)));
        Board board1 = new Board(10);

        GameplayInteractor gameplayInteractor = new GameplayInteractor(new NullGameplayProvider() {
            @Override
            public Board getBoard(Player player, Room room) {
                return room == room1 ? board1 : null;
            }

            @Override
            public Board storeBoard(Player player, Room room, Board board) {
                return room == room1 ? board1 : null;
            }
        });
        Move move = new Move();
        //set X at (1,2)
        move.setX(1);
        move.setY(2);
        move.setXValue(true);
        move.setUserId(Fakes.fakePlayer().getId());
        move.setTime(System.currentTimeMillis());
        gameplayInteractor.addMove(Fakes.fakePlayer(), room1, move);
        assertThat(board1.getValue(1, 2), is(equalTo(Board.Sign.X)));

        //check room was not changed
        assertThat(room1, is(equalTo(roomOrig)));
    }

    @Test(expected = DataStorageException.class)
    public void testGameOver() {
        Room room1 = new Room();
        room1.setId("1");

        Board board1 = new Board(10);
        //makeMove should throw DataStorageException
        board1.setFinished(true);

        GameplayInteractor gameplayInteractor = new GameplayInteractor(new NullGameplayProvider() {
            @Override
            public Board getBoard(Player player, Room room) {
                return room == room1 ? board1 : null;
            }

            @Override
            public Board storeBoard(Player player, Room room, Board board) {
                return room == room1 ? board1 : null;
            }
        });
        Move move = new Move();
        //set X at (1,2)
        move.setX(1);
        move.setY(2);
        move.setXValue(true);
        move.setUserId(Fakes.fakePlayer().getId());
        move.setTime(System.currentTimeMillis());

        gameplayInteractor.addMove(Fakes.fakePlayer(), room1, move);
        assertThat(board1.getValue(1, 2), is(equalTo(Board.Sign.X)));
    }

    @Test(expected = DataStorageException.class)
    public void testInvalidMove() {
        Room room1 = new Room();
        room1.setId("1");

        Board board1 = new Board(10);
        //makeMove should throw DataStorageException
        board1.setValue(1, 2, Board.Sign.X);

        GameplayInteractor gameplayInteractor = new GameplayInteractor(new NullGameplayProvider() {
            @Override
            public Board getBoard(Player player, Room room) {
                return room == room1 ? board1 : null;
            }

            @Override
            public Board storeBoard(Player player, Room room, Board board) {
                return room == room1 ? board1 : null;
            }
        });
        Move move = new Move();
        //set X at (1,2)
        move.setX(1);
        move.setY(2);
        move.setXValue(true);
        move.setUserId(Fakes.fakePlayer().getId());
        move.setTime(System.currentTimeMillis());

        gameplayInteractor.addMove(Fakes.fakePlayer(), room1, move);
        assertThat(board1.getValue(1, 2), is(equalTo(Board.Sign.X)));
    }
}
