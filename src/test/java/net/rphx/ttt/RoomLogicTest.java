package net.rphx.ttt;

import net.rphx.ttt.api.DataStorageException;
import net.rphx.ttt.api.RoomInteractor;
import net.rphx.ttt.api.entities.Move;
import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.stub.Fakes;
import net.rphx.ttt.stub.NullRoomProvider;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class RoomLogicTest {

    @Test
    public void testGetAllRooms() {
        Room room1 = new Room();
        room1.setId("1");
        Room room2 = new Room();
        room2.setId("2");
        Room room3 = new Room();
        room3.setId("3");
        List<Room> rooms = asList(room1, room2, room3);

        RoomInteractor roomInteractor = new RoomInteractor(new NullRoomProvider() {
            @Override
            public List<Room> getRooms(Player player) {
                return rooms;
            }
        });
        List<Room> roomsResult = roomInteractor.getRooms(Fakes.fakePlayer());
        assertThat(roomsResult, is(equalTo(rooms)));
    }

    @Test
    public void testGetRoom() {
        Room room1 = new Room();
        room1.setId("1");
        Room room2 = new Room();
        room2.setId("2");
        Room room3 = new Room();
        room3.setId("3");
        List<Room> rooms = asList(room1, room2, room3);

        RoomInteractor roomInteractor = new RoomInteractor(new NullRoomProvider() {
            @Override
            public List<Room> getRooms(Player player) {
                return rooms;
            }
        });
        Room room = roomInteractor.getRoom(Fakes.fakePlayer(), "2");
        assertThat(room, is(equalTo(room2)));
    }

    @Test(expected = DataStorageException.class)
    public void testGetRoomNotFound() {
        Room room1 = new Room();
        room1.setId("1");
        Room room2 = new Room();
        room2.setId("2");
        Room room3 = new Room();
        room3.setId("3");
        List<Room> rooms = asList(room1, room2, room3);

        RoomInteractor roomInteractor = new RoomInteractor(new NullRoomProvider() {
            @Override
            public List<Room> getRooms(Player player) {
                return rooms;
            }
        });
        roomInteractor.getRoom(Fakes.fakePlayer(), "100");
    }

    @Test
    public void testEnterRoom() {
        Room room1 = new Room();
        room1.setId("1");
        Room room2 = new Room();
        room2.setId("2");
        Room room3 = new Room();
        room3.setId("3");
        List<Room> rooms = asList(room1, room2, room3);

        RoomInteractor roomInteractor = new RoomInteractor(new NullRoomProvider() {
            @Override
            public List<Room> getRooms(Player player) {
                return rooms;
            }
        });
        assertFalse(room1.getPlayers().contains(Fakes.fakePlayer().getId()));
        roomInteractor.enterRoom(Fakes.fakePlayer(), room1.getId());
        assertTrue(room1.getPlayers().contains(Fakes.fakePlayer().getId()));
    }

    @Test
    public void testMakeMove() {
        Room room1 = new Room();
        room1.setId("1");
        Room room2 = new Room();
        room2.setId("2");
        Room room3 = new Room();
        room3.setId("3");
        List<Room> rooms = asList(room1, room2, room3);

        RoomInteractor roomInteractor = new RoomInteractor(new NullRoomProvider() {
            @Override
            public List<Room> getRooms(Player player) {
                return rooms;
            }
        });
        Move move = new Move();
        //set X at (1,2)
        move.setX(1);
        move.setY(2);
        move.setXValue(true);
        move.setUserId(Fakes.fakePlayer().getId());
        move.setTime(System.currentTimeMillis());

        roomInteractor.trackMove(Fakes.fakePlayer(), room1, move);

        assertThat(room1.getLastPlayer(), is(equalTo(Fakes.fakePlayer().getId())));
        assertThat(room1.getLastMoveTime(), is(equalTo(move.getTime())));
    }
}
