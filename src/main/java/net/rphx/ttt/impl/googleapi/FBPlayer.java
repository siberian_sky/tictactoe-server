package net.rphx.ttt.impl.googleapi;

public class FBPlayer {

    private String id;
    private String name;

    public FBPlayer() {
    }

    public FBPlayer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FBPlayer{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
