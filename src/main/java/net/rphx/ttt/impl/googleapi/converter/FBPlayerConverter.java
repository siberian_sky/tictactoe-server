package net.rphx.ttt.impl.googleapi.converter;

import com.google.api.server.spi.auth.common.User;
import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.impl.googleapi.FBPlayer;

public class FBPlayerConverter {

    public FBPlayer convert(Player player) {
        if (player == null) {
            return new FBPlayer("", "");
        }
        return new FBPlayer(player.getId(), player.getName());
    }

    public Player convert(User user, FBPlayer newPlayer) {
        return new Player(user.getId(), newPlayer.getName());
    }

    public Player convert(User user) {
        return new Player(user.getId(), "");
    }

}
