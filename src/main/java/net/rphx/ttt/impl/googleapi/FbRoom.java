package net.rphx.ttt.impl.googleapi;

import java.util.List;

public class FbRoom {
    private List<String> players;
    private String id;
    private String name;
    private long lastMoveTime;
    private String lastPlayer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }

    public long getLastMoveTime() {
        return lastMoveTime;
    }

    public void setLastMoveTime(long lastMoveTime) {
        this.lastMoveTime = lastMoveTime;
    }

    public String getLastPlayer() {
        return lastPlayer;
    }

    public void setLastPlayer(String lastPlayer) {
        this.lastPlayer = lastPlayer;
    }

    @Override
    public String toString() {
        return "FbRoom{" +
                "players=" + players +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", lastMoveTime=" + lastMoveTime +
                ", lastPlayer='" + lastPlayer + '\'' +
                '}';
    }
}
