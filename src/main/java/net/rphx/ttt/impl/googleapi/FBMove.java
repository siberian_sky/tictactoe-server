package net.rphx.ttt.impl.googleapi;

public class FBMove {
    private long time;
    private String userId;
    private String roomId;
    private int x;
    private int y;
    private boolean xValue;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public boolean isxValue() {
        return xValue;
    }

    public void setxValue(boolean xValue) {
        this.xValue = xValue;
    }

    @Override
    public String toString() {
        return "FBMove{" +
                "time=" + time +
                ", userId='" + userId + '\'' +
                ", roomId='" + roomId + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", xValue=" + xValue +
                '}';
    }
}
