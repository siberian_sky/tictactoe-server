package net.rphx.ttt.impl.googleapi.converter;

import net.rphx.ttt.api.entities.Board;
import net.rphx.ttt.api.entities.Move;
import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.impl.firebase.BoardSerializer;
import net.rphx.ttt.impl.googleapi.FBBoard;
import net.rphx.ttt.impl.googleapi.FBMove;

public class FBGameplayConverter {

    public Move convert(FBMove fbMove) {
        Move move = new Move();
        move.setTime(fbMove.getTime());
        move.setUserId(fbMove.getUserId());
        move.setX(fbMove.getX());
        move.setY(fbMove.getY());
        move.setXValue(fbMove.isxValue());
        return move;
    }

    public FBBoard convert(Room room, Board board) {
        FBBoard fbBoard = new FBBoard();
        fbBoard.setRoomId(room.getId());
        fbBoard.setState(BoardSerializer.writeBoard(board));
        fbBoard.setFinished(board.isFinished());
        fbBoard.setNextX(board.isNextX());
        return fbBoard;
    }

}
