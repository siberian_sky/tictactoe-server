package net.rphx.ttt.impl.googleapi.converter;

import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.impl.googleapi.FbRoom;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FBRoomConverter {

    public List<FbRoom> convert(List<Room> rooms) {
        if (rooms.isEmpty()) {
            return Collections.emptyList();
        }
        return rooms.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    public FbRoom convert(Room room) {
        FbRoom fbRoom = new FbRoom();
        fbRoom.setId(room.getId());
        fbRoom.setName(room.getName());
        fbRoom.setPlayers(room.getPlayers());
        fbRoom.setLastMoveTime(room.getLastMoveTime());
        fbRoom.setLastPlayer(room.getLastPlayer());
        return fbRoom;
    }

    public Room convert(FbRoom fbRoom) {
        Room room = new Room();
        room.setId(fbRoom.getId());
        room.setName(fbRoom.getName());
        room.setLastPlayer(fbRoom.getLastPlayer());
        room.setLastMoveTime(fbRoom.getLastMoveTime());
        return room;
    }

    public Room convert(String roomId) {
        Room room = new Room();
        room.setId(roomId);
        return room;
    }

}
