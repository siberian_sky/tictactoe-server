package net.rphx.ttt.impl.googleapi;

public class FBBoard {

    private String roomId;
    private String state;
    private boolean finished;
    private boolean nextX;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isNextX() {
        return nextX;
    }

    public void setNextX(boolean nextX) {
        this.nextX = nextX;
    }

    @Override
    public String toString() {
        return "FBBoard{" +
                "roomId='" + roomId + '\'' +
                ", state='" + state + '\'' +
                ", finished=" + finished +
                ", nextX=" + nextX +
                '}';
    }
}
