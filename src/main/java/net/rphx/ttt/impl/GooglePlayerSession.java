package net.rphx.ttt.impl;

import com.google.api.server.spi.auth.common.User;
import net.rphx.ttt.api.Context;
import net.rphx.ttt.api.PlayerSession;
import net.rphx.ttt.api.entities.BoardInfo;
import net.rphx.ttt.api.entities.Move;
import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.api.process.Locked;
import net.rphx.ttt.impl.googleapi.FBBoard;
import net.rphx.ttt.impl.googleapi.FBMove;
import net.rphx.ttt.impl.googleapi.FBPlayer;
import net.rphx.ttt.impl.googleapi.FbRoom;
import net.rphx.ttt.impl.googleapi.converter.FBGameplayConverter;
import net.rphx.ttt.impl.googleapi.converter.FBPlayerConverter;
import net.rphx.ttt.impl.googleapi.converter.FBRoomConverter;

import java.util.List;

import static java.util.Objects.requireNonNull;

public class GooglePlayerSession {
    private final User user;
    private final PlayerSession session;
    private final FBPlayerConverter playerConverter;
    private final FBRoomConverter roomConverter;
    private final FBGameplayConverter gameplayConverter;

    public GooglePlayerSession(ServerContext context, User user) {
        this.user = requireNonNull(user);
        ServerContext serverCtx = requireNonNull(context);
        session = new PlayerSession(new Context(serverCtx, Locked::new));
        playerConverter = new FBPlayerConverter();
        gameplayConverter = new FBGameplayConverter();
        roomConverter = new FBRoomConverter();
    }

    public FBPlayer registerPlayer(FBPlayer newPlayer) {
        Player converted = getPlayerConverter().convert(user, newPlayer);
        Player player = session.registerPlayer(converted);
        return getPlayerConverter().convert(player);
    }

    public FBPlayer getPlayer() {
        Player converted = getPlayerConverter().convert(user);
        Player player = session.getPlayer(converted);
        //this can handle nulls
        return getPlayerConverter().convert(player);
    }

    public List<FbRoom> getRooms() {
        List<Room> rooms = session.getRooms();
        return getRoomConverter().convert(rooms);
    }

    public FbRoom addNewRoom(FbRoom roomData) {
        Room roomParam = getRoomConverter().convert(roomData);
        Room room = session.addNewRoom(roomParam);
        return getRoomConverter().convert(room);
    }

    public FbRoom enterRoom(FbRoom roomData) {
        Room roomParam = getRoomConverter().convert(roomData);
        Room room = session.enterRoom(roomParam);
        return getRoomConverter().convert(room);
    }

    public FBBoard getBoard(FbRoom roomData) {
        Room roomParam = getRoomConverter().convert(roomData);
        BoardInfo boardInfo = session.getBoard(roomParam);
        return getGameplayConverter().convert(boardInfo.getRoom(), boardInfo.getBoard());
    }

    public FbRoom addMove(FBMove moveData) {
        Move moveParam = getGameplayConverter().convert(moveData);
        Room roomParam = getRoomConverter().convert(moveData.getRoomId());
        Room room = session.addMove(roomParam, moveParam);
        return getRoomConverter().convert(room);
    }

    public FBPlayerConverter getPlayerConverter() {
        return playerConverter;
    }

    public FBRoomConverter getRoomConverter() {
        return roomConverter;
    }

    public FBGameplayConverter getGameplayConverter() {
        return gameplayConverter;
    }

}
