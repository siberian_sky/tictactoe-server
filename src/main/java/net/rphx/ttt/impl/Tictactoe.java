package net.rphx.ttt.impl;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiIssuer;
import com.google.api.server.spi.config.ApiIssuerAudience;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.response.UnauthorizedException;
import net.rphx.ttt.CustomAuthenticator;
import net.rphx.ttt.impl.googleapi.FBBoard;
import net.rphx.ttt.impl.googleapi.FBMove;
import net.rphx.ttt.impl.googleapi.FBPlayer;
import net.rphx.ttt.impl.googleapi.FbRoom;

import java.util.List;
import java.util.function.Function;

@Api(name = "ttt",
        version = "v1",
        authenticators = CustomAuthenticator.class,
        issuers = @ApiIssuer(
                name = "firebase",
                issuer = "https://securetoken.google.com/jbtttapp",
                jwksUri = "https://www.googleapis.com/service_accounts/v1/metadata/x509/securetoken@system.gserviceaccount.com"
        ),
        issuerAudiences = @ApiIssuerAudience(
                name = "firebase",
                audiences = "jbtttapp"
        )
)
public class Tictactoe {
    /**
     * This instance is shared between all the clients. it requires explicit synchronization
     */
    private final TictactoeSessionManager sessionManager;

    public Tictactoe() {
        sessionManager = new TictactoeSessionManager();
    }

    private void checkUserAuthenticated(User user) throws UnauthorizedException {
        if (user == null) {
            throw new UnauthorizedException("Invalid credentials");
        }
    }

    private <T> T execute(User user, Function<GooglePlayerSession, T> function) throws UnauthorizedException {
        checkUserAuthenticated(user);
        return sessionManager.executeInSession(user, function);
    }

    /**
     * Creates new user from the provided data
     */
    @ApiMethod(name = "players.create")
    public FBPlayer registerPlayer(User user, FBPlayer player) throws UnauthorizedException {
        return execute(user, session -> session.registerPlayer(player));
    }

    /**
     * loads existing user from storage
     */
    @ApiMethod(name = "players.get")
    public FBPlayer getPlayer(User user) throws UnauthorizedException {
        return execute(user, GooglePlayerSession::getPlayer);
    }

    /**
     * Loads all rooms available to currently authorized user
     */
    @ApiMethod(name = "rooms.get")
    public List<FbRoom> getRooms(User user) throws UnauthorizedException {
        return execute(user, GooglePlayerSession::getRooms);
    }

    /**
     * Create new room
     */
    @ApiMethod(name = "rooms.add")
    public FbRoom addRoom(User user, FbRoom room) throws UnauthorizedException {
        return execute(user, session -> session.addNewRoom(room));
    }

    /**
     * Enter existing room
     */
    @ApiMethod(name = "rooms.enter")
    public FbRoom enterRoom(User user, FbRoom room) throws UnauthorizedException {
        return execute(user, session -> session.enterRoom(room));
    }

    @ApiMethod(name = "board.move")
    public FbRoom addMove(User user, FBMove move) throws UnauthorizedException {
        return execute(user, session -> session.addMove(move));
    }

    @ApiMethod(name = "board.get", httpMethod = "POST")
    public FBBoard getBoard(User user, FbRoom room) throws UnauthorizedException {
        return execute(user, session -> session.getBoard(room));
    }

}
