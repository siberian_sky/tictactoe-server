package net.rphx.ttt.impl;

import com.google.api.server.spi.auth.common.User;
import net.rphx.ttt.api.process.SessionManager;

public class TictactoeSessionManager extends SessionManager<ServerContext, User, GooglePlayerSession> {

    public TictactoeSessionManager() {
        super(ServerContext::new, User::getId, GooglePlayerSession::new);
        //todo: check for session timeout
        //todo: protect from multiple session creation
    }

}
