package net.rphx.ttt.impl.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import net.rphx.ttt.api.entities.Board;
import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.api.storage.GameplayProvider;

import java.util.HashMap;
import java.util.Map;

public class FirebaseGameplayProvider implements GameplayProvider {

    @Override
    public Board getBoard(Player player, Room room) {
        DatabaseReference ref = getBoards();
        Query query = ref.child(room.getId());
        return FirebaseUtils.loadBlocking(Board.class, query, snapshot -> fromMap((Map<String, Object>) snapshot.getValue()), null);
    }

    @Override
    public Board storeBoard(Player player, Room room, Board board) {
        DatabaseReference ref = getBoards().child(room.getId());
        FirebaseUtils.updateBlocking(ref, toMap(board));
        return board;
    }

    private DatabaseReference getBoards() {
        return FirebaseDatabase
                .getInstance()
                .getReference("/boards");
    }

    private Board fromMap(Map<String, Object> map) {
        int size = Integer.parseInt(map.get("size").toString());
        Board board = new Board(size);
        String cells = (String) map.get("cells");
        BoardSerializer.readBoard(board, cells);
        board.setFinished(Boolean.parseBoolean(map.get("finished").toString()));
        board.setNextX(Boolean.parseBoolean(map.get("nextX").toString()));
        return board;
    }

    private Map<String, Object> toMap(Board board) {
        Map<String, Object> result = new HashMap<>();
        result.put("size", board.getSize());
        result.put("finished", board.isFinished());
        result.put("cells", BoardSerializer.writeBoard(board));
        result.put("nextX", board.isNextX());
        return result;
    }

}
