package net.rphx.ttt.impl.firebase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.api.storage.RoomProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Collections.emptyList;

/**
 * Interacts with the database
 */
public class FirebaseRoomProvider implements RoomProvider {

    @SuppressWarnings("unchecked")
    @Override
    public List<Room> getRooms(Player player) {
        return FirebaseUtils.loadBlocking(List.class, getDB().orderByKey(), snapshot -> {
            List<Room> rooms = new ArrayList<>();
            for (DataSnapshot ds : snapshot.getChildren()) {
                Room room = fromMap((Map<String, Object>) ds.getValue());
                room.setId(ds.getKey());
                rooms.add(room);
            }
            return rooms;
        }, emptyList());
    }

    @Override
    public Room newRoom(Player player, Room room) {
        AtomicReference<String> keyRef = new AtomicReference<>();
        Map<String, Object> map = FirebaseUtils.insertBlocking(getDB(), toMap(room), (val, key) -> {
            keyRef.set(key);
            return val;
        });
        Room newRoom = fromMap(map);
        newRoom.setId(keyRef.get());
        return newRoom;
    }

    @Override
    public Room store(Player player, Room room) {
        FirebaseUtils.updateBlocking(getDB().child(room.getId()), toMap(room));
        return room;
    }

    private Map<String, Object> toMap(Room room) {
        Map<String, Object> result = new HashMap<>();
        result.put("name", room.getName());
        result.put("lastMoveTime", room.getLastMoveTime());
        result.put("lastPlayer", room.getLastPlayer());
        result.put("players", room.getPlayers());
        return result;
    }

    private Room fromMap(Map<String, Object> map) {
        Room room = new Room();
        room.setId((String) map.get("id"));
        room.setName((String) map.get("name"));
        room.setLastMoveTime(getLong(map.get("lastMoveTime")));
        room.setLastPlayer((String) map.get("lastPlayer"));
        List<String> players = (List<String>) map.get("players");
        if (players != null) {
            players.forEach(room::addPlayer);
        }
        return room;
    }

    private long getLong(Object value) {
        try {
            return Long.valueOf(value.toString());
        } catch (RuntimeException ignored) {
            return 0;
        }
    }

    private DatabaseReference getDB() {
        return FirebaseDatabase
                .getInstance()
                .getReference("/rooms");
    }
}
