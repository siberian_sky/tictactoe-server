package net.rphx.ttt.impl.firebase;

import com.google.api.core.ApiFuture;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import net.rphx.ttt.api.DataStorageException;

import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FirebaseUtils {
    private static final Logger logger = Logger.getLogger(FirebaseUtils.class.getName());

    private FirebaseUtils() {
    }

    public static <T> T loadBlocking(Class<T> clazz, Query query, Function<DataSnapshot, T> processor, T defaultValue) {
        debugLog(() -> "loadBlocking " + clazz);
        final CountDownLatch latch = new CountDownLatch(1);
        AtomicReference<T> loaderRef = new AtomicReference<>(defaultValue);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                debugLog(() -> "onDataChange " + clazz + ' ' + snapshot);
                try {
                    if (snapshot.exists()) {
                        T apply = processor.apply(snapshot);
                        loaderRef.set(apply);
                        debugLog(() -> "Result set to " + loaderRef.get());
                    }
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Couldn't parse data from record", e);
                    throw new DataStorageException(e);
                } finally {
                    debugLog(() -> "Call countDown to stop waiting");
                    latch.countDown();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                throw new DataStorageException(error.toException());
            }
        });
        debugLog(() -> "Wait for data on latch");
        try {
            latch.await();
        } catch (InterruptedException e) {
            //restore interrupt flag
            Thread.currentThread().interrupt();
            throw new DataStorageException(e);
        }
        debugLog(() -> "Data loaded");
        return loaderRef.get();
    }

    private static void debugLog(Supplier<String> supplier) {
        logger.log(Level.FINE, supplier);
    }

    public static <T> T insertBlocking(DatabaseReference db, T data, BiFunction<T, String, T> insertListener) {
        DatabaseReference newRow = db.push();
        T dataToInsert = insertListener.apply(data, newRow.getKey());
        return executeBlocking(newRow, dataToInsert);
    }

    public static <T> T executeBlocking(DatabaseReference newRow, T dataToInsert) {
        ApiFuture<Void> voidApiFuture = newRow.setValueAsync(dataToInsert);
        try {
            voidApiFuture.get();
            return dataToInsert;
        } catch (InterruptedException e) {
            //restore interrupt flag
            Thread.currentThread().interrupt();
            throw new DataStorageException(e);
        } catch (ExecutionException e) {
            throw new DataStorageException(e);
        }
    }

    public static void updateBlocking(DatabaseReference db, Map<String, Object> data) {
        ApiFuture<Void> voidApiFuture = db.updateChildrenAsync(data);
        try {
            voidApiFuture.get();
        } catch (InterruptedException e) {
            //restore interrupt flag
            Thread.currentThread().interrupt();
            throw new DataStorageException(e);
        } catch (ExecutionException e) {
            throw new DataStorageException(e);
        }
    }

}
