package net.rphx.ttt.impl.firebase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import net.rphx.ttt.api.DataStorageException;
import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.api.storage.PlayerProvider;

import java.util.Iterator;

/**
 * Stores players in Firebase database
 */
public class FirebasePlayerProvider implements PlayerProvider<String> {

    @Override
    public Player createPlayer(Player player) {
        checkPlayerExists(player);
        return FirebaseUtils.insertBlocking(getDB(), player, (pl, key) -> pl);
    }

    private void checkPlayerExists(Player player) {
        DatabaseReference ref = getDB();
        if (getPlayer(ref.orderByChild("id").equalTo(player.getId())) != null) {
            throw new DataStorageException("User with uid already exists: " + player.getId());
        }
        if (getPlayer(ref.orderByChild("name").equalTo(player.getName())) != null) {
            throw new DataStorageException("User with name already exists: " + player.getName());
        }
    }

    @Override
    public Player getPlayer(String uid) {
        DatabaseReference ref = getDB();
        Query query = ref.orderByChild("id").equalTo(uid);
        return getPlayer(query);
    }

    private Player getPlayer(Query query) {
        return FirebaseUtils.loadBlocking(Player.class, query, snapshot -> {
            Iterator<DataSnapshot> iterator = snapshot.getChildren().iterator();
            if (iterator.hasNext()) {
                return iterator.next().getValue(Player.class);
            } else {
                return null;
            }
        }, null);
    }

    private DatabaseReference getDB() {
        return FirebaseDatabase
                .getInstance()
                .getReference("/players");
    }
}
