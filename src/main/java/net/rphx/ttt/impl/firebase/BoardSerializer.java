package net.rphx.ttt.impl.firebase;

import net.rphx.ttt.api.entities.Board;

public class BoardSerializer {

    private BoardSerializer() {
    }

    public static void readBoard(Board board, String string) {
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                board.setValue(i, j, toSign(string.charAt(i * board.getSize() + j)));
            }
        }
    }

    public static String writeBoard(Board board) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                sb.append(toStr(board.getValue(i, j)));
            }
        }
        return sb.toString();
    }

    private static char toStr(Board.Sign sign) {
        switch (sign) {
            case X:
                return 'x';
            case O:
                return 'o';
            default:
                return '-';
        }
    }

    private static Board.Sign toSign(char charSign) {
        switch (charSign) {
            case 'x':
                return Board.Sign.X;
            case 'o':
                return Board.Sign.O;
            default:
                return Board.Sign.NONE;
        }
    }
}
