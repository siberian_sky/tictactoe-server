package net.rphx.ttt.impl;

import net.rphx.ttt.api.storage.DataProviders;
import net.rphx.ttt.api.GameplayInteractor;
import net.rphx.ttt.api.RoomInteractor;
import net.rphx.ttt.api.storage.PlayerProvider;
import net.rphx.ttt.impl.firebase.FirebaseGameplayProvider;
import net.rphx.ttt.impl.firebase.FirebasePlayerProvider;
import net.rphx.ttt.impl.firebase.FirebaseRoomProvider;

public class ServerContext implements DataProviders {
    private final PlayerProvider<String> playerProvider;
    private final RoomInteractor roomInteractor;
    private final GameplayInteractor gameplayInteractor;

    public ServerContext() {
        playerProvider = new FirebasePlayerProvider();
        gameplayInteractor = new GameplayInteractor(new FirebaseGameplayProvider());
        roomInteractor = new RoomInteractor(new FirebaseRoomProvider());
    }

    @Override
    public PlayerProvider<String> getPlayerProvider() {
        return playerProvider;
    }

    @Override
    public RoomInteractor getRoomInteractor() {
        return roomInteractor;
    }

    @Override
    public GameplayInteractor getGameplayInteractor() {
        return gameplayInteractor;
    }
}
