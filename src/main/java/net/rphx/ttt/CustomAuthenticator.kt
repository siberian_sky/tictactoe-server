package net.rphx.ttt

import com.google.api.server.spi.auth.common.User
import com.google.api.server.spi.config.Authenticator
import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseToken
import java.io.File
import java.io.FileInputStream
import java.util.concurrent.ExecutionException
import java.util.logging.Level
import java.util.logging.Logger
import javax.servlet.http.HttpServletRequest

class CustomAuthenticator : Authenticator {

    override fun authenticate(httpServletRequest: HttpServletRequest): User? {
        val authorizationHeader = httpServletRequest.getHeader("Authorization")
        logger.info("auth header: " + authorizationHeader)
        //verify
        val id = authorizationHeader.replace("Bearer ", "").trim { it <= ' ' }
        val firebaseToken: FirebaseToken
        try {
            firebaseToken = FirebaseAuth.getInstance().verifyIdTokenAsync(id).get()
        } catch (e: ExecutionException) {
            logger.log(Level.SEVERE, "Couldn't verify token", e)
            return null
        } catch (e: InterruptedException) {
            logger.log(Level.SEVERE, "Couldn't verify token", e)
            return null
        }

        logger.info("Tkn: " + firebaseToken.uid)
        return User(firebaseToken.uid, firebaseToken.email)
    }

    companion object {
        private val logger = Logger.getLogger(CustomAuthenticator::class.java.name)

        init {
            try {
                val options = FirebaseOptions.Builder()
                        .setCredentials(GoogleCredentials.fromStream(FileInputStream(File(System.getProperty("service.json.file")))))
                        .setDatabaseUrl("https://jbtttapp.firebaseio.com")
                        .build()

                FirebaseApp.initializeApp(options)

            } catch (e: Exception) {
                logger.log(Level.SEVERE, e.toString(), e)
            }

        }
    }
}
