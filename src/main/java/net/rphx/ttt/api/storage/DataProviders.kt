package net.rphx.ttt.api.storage

import net.rphx.ttt.api.GameplayInteractor
import net.rphx.ttt.api.RoomInteractor

interface DataProviders {

    val playerProvider: PlayerProvider<String>

    val roomInteractor: RoomInteractor

    val gameplayInteractor: GameplayInteractor
}
