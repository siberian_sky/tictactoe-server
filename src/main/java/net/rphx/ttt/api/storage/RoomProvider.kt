package net.rphx.ttt.api.storage

import net.rphx.ttt.api.entities.Player
import net.rphx.ttt.api.entities.Room

interface RoomProvider {
    /**
     * List all available rooms
     *
     * @param player
     * @return
     */
    fun getRooms(player: Player): List<Room>

    fun newRoom(player: Player, room: Room): Room

    fun store(player: Player, room: Room): Room

}
