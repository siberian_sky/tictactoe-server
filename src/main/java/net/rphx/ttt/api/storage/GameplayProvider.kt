package net.rphx.ttt.api.storage


import net.rphx.ttt.api.entities.Board
import net.rphx.ttt.api.entities.Player
import net.rphx.ttt.api.entities.Room

interface GameplayProvider {

    fun getBoard(player: Player, room: Room): Board?

    fun storeBoard(player: Player, room: Room, board: Board): Board
}
