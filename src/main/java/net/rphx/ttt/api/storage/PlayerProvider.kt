package net.rphx.ttt.api.storage

import net.rphx.ttt.api.entities.Player

/**
 * Player loader
 */
interface PlayerProvider<in T> {

    fun createPlayer(player: Player): Player

    fun getPlayer(id: T): Player?
}
