package net.rphx.ttt.api

import net.rphx.ttt.api.entities.Board

object WinChecker {

    fun checkWinner(board: Board, lastMove: Board.Sign): Board.Sign? {
        var d1Win = true
        var d2Win = true
        var freeCells = false
        var i = 0
        val n = board.size
        while (i < n) {
            var verticalWin = true
            var horizontalWin = true
            for (j in 0 until n) {
                if (board.getValue(i, j) !== lastMove) {
                    horizontalWin = false
                }
                if (board.getValue(j, i) !== lastMove) {
                    verticalWin = false
                }
                if (board.getValue(i, j) === Board.Sign.NONE) {
                    freeCells = true
                }
            }
            if (horizontalWin || verticalWin) {
                return lastMove
            }
            if (board.getValue(i, i) !== lastMove) {
                d1Win = false
            }
            if (board.getValue(i, n - i - 1) !== lastMove) {
                d2Win = false
            }
            i++
        }
        if (d1Win || d2Win) {
            return lastMove
        }
        return if (freeCells) null else Board.Sign.NONE
    }
}
