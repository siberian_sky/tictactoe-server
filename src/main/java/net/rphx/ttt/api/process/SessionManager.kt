package net.rphx.ttt.api.process


import java.util.concurrent.ConcurrentHashMap
import java.util.function.BiFunction
import java.util.function.Function
import java.util.function.Supplier

open class SessionManager<C, in U, S>(contextProvider: Supplier<C>,
                                      private val sessionIdCreator: SessionIdCreator<U>,
                                      private val playerSessionProvider: BiFunction<C, U, S>) {

    private val context: C = contextProvider.get()
    private val sessions = ConcurrentHashMap<String, Locked<S>>()

    fun <T> executeInSession(user: U, function: Function<S, T>): T {
        val session = sessions.computeIfAbsent(sessionIdCreator.getSessionId(user)) { Locked(playerSessionProvider.apply(context, user)) }
        return session.execute(function)
    }
}
