package net.rphx.ttt.api.process

import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import java.util.function.Function

class Locked<U>(private val target: U) : Executor<U> {
    private val lock: Lock

    init {
        lock = ReentrantLock()
    }

    override fun <T> execute(function: Function<U, T>): T {
        lock.lock()
        try {
            return function.apply(target)
        } finally {
            lock.unlock()
        }
    }
}
