package net.rphx.ttt.api.process

interface SessionIdCreator<in T> {
    fun getSessionId(param: T): String
}
