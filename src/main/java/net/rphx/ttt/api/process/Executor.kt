package net.rphx.ttt.api.process

import java.util.function.Function

interface Executor<U> {
    fun <T> execute(function: Function<U, T>): T
}
