package net.rphx.ttt.api

import net.rphx.ttt.api.entities.BoardInfo
import net.rphx.ttt.api.entities.Move
import net.rphx.ttt.api.entities.Player
import net.rphx.ttt.api.entities.Room
import net.rphx.ttt.api.storage.DataProviders

class Gameplay(private val dataProviders: DataProviders) {

    private val roomInteractor: RoomInteractor
        get() = dataProviders.roomInteractor

    private val gameplayInteractor: GameplayInteractor
        get() = dataProviders.gameplayInteractor

    fun getRooms(player: Player): List<Room> {
        val rooms = roomInteractor.getRooms(player)
        checkNotNull(rooms, "Couldn't load rooms")
        return rooms
    }

    fun addNewRoom(player: Player, roomData: Room): Room {
        val room = roomInteractor.addNewRoom(player, roomData.name!!)
        checkNotNull(room, "Couldn't create room: " + roomData)
        return room
    }

    fun enterRoom(player: Player, roomData: Room): Room {
        val room = roomInteractor.enterRoom(player, roomData.id!!)
        checkNotNull(room, "Couldn't update room: " + roomData)
        return room
    }

    fun getBoard(player: Player, roomData: Room): BoardInfo {
        val room = roomInteractor.getRoom(player, roomData.id!!)
        checkNotNull(room, "Couldn't load room: " + roomData.id!!)
        val board = gameplayInteractor.getBoard(player, room)
        return BoardInfo(board, room)
    }

    fun addMove(player: Player, roomData: Room, moveData: Move): Room {
        val room = roomInteractor.getRoom(player, roomData.id!!)
        checkNotNull(room, "Couldn't load room: " + roomData.id!!)
        checkLastMove(room, player)
        val move = gameplayInteractor.addMove(player, room, moveData)
        checkNotNull(room, "Couldn't add move: " + moveData)
        roomInteractor.trackMove(player, room, move)
        return room
    }

    private fun checkLastMove(room: Room, player: Player) {
        if (player.id == room.lastPlayer) {
            throw ProcessingException("Please wait your turn")
        }
        if (!room.getPlayers().contains(player.id)) {
            throw DataStorageException(String.format("User %s does not play in room %s", player.name, room.name))
        }
    }

    private fun <T> checkNotNull(param: T?, message: String): T {
        if (param == null) {
            throw ProcessingException(message)
        }
        return param
    }
}
