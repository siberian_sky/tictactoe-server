package net.rphx.ttt.api;

import net.rphx.ttt.api.entities.BoardInfo;
import net.rphx.ttt.api.entities.Move;
import net.rphx.ttt.api.entities.Player;
import net.rphx.ttt.api.entities.Room;
import net.rphx.ttt.api.storage.PlayerProvider;

import java.util.List;
import java.util.function.Function;

public class PlayerSession {
    private Player player;
    private final Context context;

    public PlayerSession(Context context) {
        this.context = context;
    }

    public Player registerPlayer(Player newPlayer) {
        checkNotNull(newPlayer, "Parameter cannot be null");
        if (player != null) {
            throw new ProcessingException("Player is already created: " + player);
        }
        player = getPlayerProvider().createPlayer(newPlayer);
        checkNotNull(player, "Couldn't create player " + newPlayer);
        return player;
    }

    public Player getPlayer(Player playerData) {
        if (player == null) {
            player = getPlayerProvider().getPlayer(playerData.getId());
        }
        return player;
    }

    public List<Room> getRooms() {
        checkPlayerAuthenticated();
        return execInGameplay(gameplay -> gameplay.getRooms(player));
    }

    public Room addNewRoom(Room roomData) {
        checkPlayerAuthenticated();
        return execInGameplay(gameplay -> gameplay.addNewRoom(player, roomData));
    }

    public Room enterRoom(Room roomData) {
        checkPlayerAuthenticated();
        return execInGameplay(gameplay -> gameplay.enterRoom(player, roomData));
    }

    public BoardInfo getBoard(Room roomData) {
        checkPlayerAuthenticated();
        return execInGameplay(gameplay -> gameplay.getBoard(player, roomData));
    }

    public Room addMove(Room roomData, Move moveData) {
        checkPlayerAuthenticated();
        return execInGameplay(gameplay -> gameplay.addMove(player, roomData, moveData));
    }

    private <T> T execInGameplay(Function<Gameplay, T> function) {
        return context.getGameplay().execute(function);
    }

    private PlayerProvider<String> getPlayerProvider() {
        return context.getProviders().getPlayerProvider();
    }

    private void checkPlayerAuthenticated() {
        if (player == null) {
            throw new ProcessingException("Player is not authenticated");
        }
    }

    private <T> T checkNotNull(T param, String message) {
        if (param == null) {
            throw new ProcessingException(message);
        }
        return param;
    }

}
