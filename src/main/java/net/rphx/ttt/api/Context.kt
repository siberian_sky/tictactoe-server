package net.rphx.ttt.api

import net.rphx.ttt.api.process.Executor
import net.rphx.ttt.api.storage.DataProviders

import java.util.function.Function

class Context(val providers: DataProviders, transactionalSupplier: Function<Gameplay, Executor<Gameplay>>) {
    val gameplay: Executor<Gameplay> = transactionalSupplier.apply(Gameplay(providers))
}
