package net.rphx.ttt.api

import net.rphx.ttt.api.entities.Move
import net.rphx.ttt.api.entities.Player
import net.rphx.ttt.api.entities.Room
import net.rphx.ttt.api.storage.RoomProvider

class RoomInteractor(private val roomProvider: RoomProvider) {

    fun getRooms(player: Player): List<Room> {
        return loadRooms(player)
    }

    fun getRoom(player: Player, roomId: String): Room {
        return findRoom(loadRooms(player), roomId)
    }

    private fun loadRooms(player: Player): List<Room> {
        return roomProvider.getRooms(player)
    }

    private fun findRoom(rooms: List<Room>, roomId: String): Room {
        return rooms.stream()
                .filter { room -> room.id == roomId }
                .findAny()
                .orElseThrow { DataStorageException("Room with id not found: " + roomId) }
    }

    fun addNewRoom(player: Player, name: String): Room {
        val room = Room()
        room.name = name
        val createdRoom = roomProvider.newRoom(player, room)
        return enterRoom(player, createdRoom.id!!)
    }

    fun enterRoom(player: Player, roomId: String): Room {
        val rooms = loadRooms(player)
        val room = findRoom(rooms, roomId)
        val playerId = player.id
        if (room.getPlayers().contains(playerId)) {
            return room
        }
        //remove player from other rooms
        rooms.stream()
                .filter { r -> r.getPlayers().contains(playerId) }
                .findAny()
                .ifPresent { r ->
                    r.removePlayer(player)
                    roomProvider.store(player, r)
                }
        //add player to this room
        room.addPlayer(player)
        roomProvider.store(player, room)
        return room
    }

    fun trackMove(player: Player, room: Room, move: Move) {
        room.lastMoveTime = move.time
        room.lastPlayer = player.id

        roomProvider.store(player, room)
    }

}
