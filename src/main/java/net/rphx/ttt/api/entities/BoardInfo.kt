package net.rphx.ttt.api.entities

class BoardInfo(val board: Board, val room: Room) {

    override fun toString(): String {
        return "BoardInfo{" +
                "board=" + board +
                ", room=" + room +
                '}'
    }
}
