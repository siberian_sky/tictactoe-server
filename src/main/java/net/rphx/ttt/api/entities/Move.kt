package net.rphx.ttt.api.entities

class Move {
    var time: Long = 0
    var userId: String = ""
    var x: Int = 0
    var y: Int = 0
    /**
     * true = x, false = 0
     */
    var xValue: Boolean = false

    override fun toString(): String {
        return "Move{" +
                "time=" + time +
                ", userId='" + userId + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", xValue=" + xValue +
                '}'
    }
}
