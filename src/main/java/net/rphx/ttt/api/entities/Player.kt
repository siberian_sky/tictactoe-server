package net.rphx.ttt.api.entities

/**
 * entity class
 */
data class Player(val id: String, val name: String) {

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Player
        return id == other.id
    }
}
