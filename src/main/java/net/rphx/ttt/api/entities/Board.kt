package net.rphx.ttt.api.entities

import java.util.*

inline fun <reified INNER> array2d(sizeOuter: Int, sizeInner: Int, noinline innerInit: (Int) -> INNER): Array<Array<INNER>>
        = Array(sizeOuter) { Array(sizeInner, innerInit) }

/**
 * Entity class
 * Playing board with specified size
 */
class Board(size: Int) {

    private val board: Array<Array<Sign>> = array2d(size, size) { Sign.NONE }
    var isFinished: Boolean = false
    var isNextX: Boolean = true

    val size get() = board.size

    enum class Sign {
        X, O, NONE
    }

    fun getValue(x: Int, y: Int) = board[x][y]

    fun setValue(x: Int, y: Int, value: Sign) {
        board[x][y] = value
    }

    override fun toString(): String {
        return "Board{" +
                "board=" + Arrays.toString(board) +
                ", finished=" + isFinished +
                ", nextX=" + isNextX +
                '}'
    }
}
