package net.rphx.ttt.api.entities

import java.util.*

/**
 * Entity class
 * Playing board with players
 */
class Room {
    var id: String? = null
    var name: String? = null
    private val players = ArrayList<String>()
    var lastMoveTime: Long = 0
    var lastPlayer: String? = null

    fun getPlayers(): List<String> {
        return Collections.unmodifiableList(players)
    }

    fun addPlayer(player: Player) {
        players.add(player.id)
    }

    fun addPlayer(playerId: String) {
        players.add(playerId)
    }

    fun removePlayer(player: Player) {
        players.remove(player.id)
    }

    override fun toString(): String {
        return "Room{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", players=" + players +
                ", lastMoveTime=" + lastMoveTime +
                ", lastPlayer='" + lastPlayer + '\'' +
                '}'
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }
        val room = other as Room?
        return lastMoveTime == room!!.lastMoveTime &&
                id == room.id &&
                name == room.name &&
                players == room.players &&
                lastPlayer == room.lastPlayer
    }

    override fun hashCode(): Int {
        return Objects.hash(id, name, players, lastMoveTime, lastPlayer)
    }
}
