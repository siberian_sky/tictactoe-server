package net.rphx.ttt.api


import net.rphx.ttt.api.WinChecker.checkWinner
import net.rphx.ttt.api.entities.Board
import net.rphx.ttt.api.entities.Move
import net.rphx.ttt.api.entities.Player
import net.rphx.ttt.api.entities.Room
import net.rphx.ttt.api.storage.GameplayProvider

class GameplayInteractor(private val provider: GameplayProvider) {
    private val BOARD_SIZE = 10

    fun addMove(player: Player, room: Room, move: Move): Move {
        val board = getBoard(player, room)
        if (board.isFinished) {
            throw DataStorageException(String.format("Game is over in room %s", room.name))
        }
        if (board.getValue(move.x, move.y) !== Board.Sign.NONE) {
            throw DataStorageException("The cell is already occupied")
        }
        move.time = System.currentTimeMillis()
        move.userId = player.id
        updateBoard(board, move)
        provider.storeBoard(player, room, board)
        return move
    }

    private fun updateBoard(board: Board, lastMove: Move) {
        val winner = if (lastMove.xValue) Board.Sign.X else Board.Sign.O
        board.setValue(lastMove.x, lastMove.y, winner)

        val sign = checkWinner(board, winner)
        board.isFinished = sign != null && sign !== Board.Sign.NONE
        board.isNextX = !lastMove.xValue
    }

    fun getBoard(player: Player, room: Room): Board {
        var board = provider.getBoard(player, room)
        if (board == null) {
            board = provider.storeBoard(player, room, Board(BOARD_SIZE))
        }
        return board
    }

}
